// GET REQUEST
function getTodos() {
    axios
        .get('http://localhost:8080/checkins', {
            timeout: 5000
        })
        .then(res => showOutput(res))
        .catch(err => console.error(err));
}
// Show output in browser
function showOutput(res) {
    /*document.getElementById('res').innerHTML = `
    <div class="card mt-3">
      <div class="card-header">
        Data
      </div>
      <div class="card-body">
        <pre>${JSON.stringify(res.data, null, 2)}</pre>
      </div>
    </div>
  `;*/
    console.log(res.data);
    if (res.data.length === 0) {
        $('<div>').text('No checkins available').appendTo('#res');
    }
    for (var i = 0; i < res.data.length; i++) {
        const checkinEl = $('<div>').addClass('card p-4 my-4');
        const h3 = $('<h3>').text(res.data[i].name).appendTo(checkinEl);
        const starRating = $('<div>').addClass('star-rating');
        const checkinRating = res.data[i].rating * 20;
        $('<div>').css('width', checkinRating + '%').appendTo(starRating);
        starRating.appendTo(h3);
        $('<p>').text(res.data[i].review).appendTo(checkinEl);
        $('#res').append(checkinEl);
    }
}
// Event listeners
document.getElementById('get').addEventListener('click', getTodos);


const toTop = document.querySelector(".to-top");

window.addEventListener("scroll", () => {
    if (window.pageYOffset > 100) {
        toTop.classList.add("active");
    } else {
        toTop.classList.remove("active");
    }
})

