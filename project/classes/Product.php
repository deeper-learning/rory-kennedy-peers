<?php

class Product
{
    public int $id;
    public string $title;
    public string $description;
    public string $imagePath;
    public string $genre;
    public ?float $average_rating;
    /** @var CheckIn[] */
    public array $checkIns;
}
